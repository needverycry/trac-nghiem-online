const questionList = [];

// lấy data từ db
// arrow function
const getData = () => {
    axios({
        method: 'GET',
        url: '../../assets/DeThiTracNghiem.json'
    })
        .then(function (res) {
            mapDataToObject(res.data);
            renderQuestion();
            console.log(res.data)
        })
        .catch(function (err) {
            console.log(err);
        })
};

const mapDataToObject = data => {
    for (let question of data) {
        //destructuring: boc tach phan tu
        const { questionType, _id, content, answers } = question;
        // => const questionType:

        let newQuestion = {};
        if (questionType === 1) {
            newQuestion = new MultipleChoice(
                questionType,
                _id,
                content,
                answers
            );
        } else {
            newQuestion = new FillInBlank(
                questionType,
                _id,
                content,
                answers
            );
        }
        questionList.push(newQuestion);
    }
    console.log(questionList);
}

const renderQuestion = () => {
    let content = '';
    for (let i in questionList) {
        content += questionList[i].render(i * 1 + 1);
    }

    document.getElementById('content').innerHTML = content;
}
getData();