class FillInBlank extends Question {
    // default value, rest parameter
    constructor(...args) {
        super(...agr);
    }

    render(index) {
        // string temple
        return `
            <div>
                <h4>Câu hỏi ${index} : ${this.content}</h4>
                <input type='text'/>
            </div>
        `
    }
}
