class MultipleChoice extends Question {
    // rest parameter
    constructor(...args) {
        super(...args); // => super(type,id,content, answer);
    }
    renderAnswer() {
        let answerText = '';
        for (let ans of this.answers) {
            answerText += `
                <div>
                    <input type="radio" name="${this._id}"/>
                    <span>${ans.content}</span>
                </div>
            `
        }
        return answerText;
    }
    render(index) {
        // string temple
        return `
            <div>
                <h4>Câu hỏi ${index} : ${this.content}</h4>
                ${this.renderAnswer()}
            </div>
        `
    }
}
